import logging
import zipfile
from string import ascii_letters
from uuid import uuid4

import click
import lxml.etree as et

import os
from random import randint, randrange, sample

logger = logging.getLogger(__name__)


def _build_xml(id_, level, objects):
    builder = et.TreeBuilder()
    builder.start('root', {})
    builder.start('var', {'name': 'id', 'value': id_})
    builder.end('var')
    builder.start('var', {'name': 'level', 'value': level})
    builder.end('var')
    builder.start('objects', {})
    for obj in objects:
        builder.start('object', {'name': obj})
        builder.end('object')
    builder.end('objects')
    builder.end('root')
    root = builder.close()
    return root


def _create_sample():
    id_ = uuid4().hex
    level = str(randint(1, 11))
    objects = [''.join(sample(ascii_letters, 4)) for _ in range(randrange(10))]
    return id_, level, objects


def generate(work_dir, archive_amount, files_per_archive):
    """
    Generates sample data.

    Args:
        work_dir (str): Working directory.
        archive_amount (int): Amount of archives.
        files_per_archive (int): Amount of files per archive.
    """
    assert archive_amount > 0
    assert files_per_archive > 0

    logger.info('Start sample generation. Archive amount: %s, Files per archive: %s',
                archive_amount, files_per_archive)

    os.makedirs(work_dir, exist_ok=True)

    for i in range(archive_amount):
        archive_name = os.path.join(work_dir, '%s.zip' % i)
        with zipfile.ZipFile(archive_name, 'w', compression=zipfile.ZIP_DEFLATED) as z:
            for j in range(files_per_archive):
                z.writestr('%s.xml' % j, et.tostring(_build_xml(*_create_sample()), pretty_print=True))

    logger.info('Done. Result files written to "%s"' % work_dir)


@click.command()
@click.option('--work_dir', default='work_dir', type=click.Path(), show_default=True, help='Working directory')
@click.option('--archive_amount', default=50, type=click.IntRange(min=1), show_default=True,
              help='Amount of archives')
@click.option('--files_per_archive', default=100, type=click.IntRange(min=1), show_default=True,
              help='Files amount per archive')
def cli(work_dir, archive_amount, files_per_archive):
    generate(work_dir, archive_amount, files_per_archive)


if __name__ == '__main__':
    import sys

    logging.basicConfig(level=logging.INFO, format='%(message)s', stream=sys.stdout)
    cli()
