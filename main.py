import concurrent.futures
import glob
import logging
import zipfile

import click
import lxml.etree as et

import os

logger = logging.getLogger(__name__)


def _parse(data_str):
    root = et.fromstring(data_str)
    id_ = root.find("./var[@name='id']").get('value')
    level = root.find("./var[@name='level']").get('value')
    objects = list(o.get('name') for o in root.findall("./objects/object"))

    return id_, level, objects


def _process_zip(filename):
    res1 = []
    res2 = []
    with zipfile.ZipFile(filename, compression=zipfile.ZIP_DEFLATED) as z:
        for filename in z.namelist():
            id_, level, objects = _parse(z.read(filename))
            res1.append((id_, level))
            res2.extend((id_, obj) for obj in objects)
    return res1, res2


def main(in_dir, out_dir):
    """
    Load input data and generate result csv files.

    Args:
        in_dir (str): Directory with input files.
        out_dir (str): Directory where result files will be written to.
    """
    # prepare result files
    os.makedirs(out_dir, exist_ok=True)
    levels_res_file = os.path.join(out_dir, 'levels.csv')
    objects_res_file = os.path.join(out_dir, 'objects.csv')
    with open(levels_res_file, 'w') as f:
        f.write('id, level\n')
    with open(objects_res_file, 'w') as f:
        f.write('id, object\n')

    # get all zip
    logger.info('Read files from "%s"', in_dir)
    files = glob.glob(os.path.join(in_dir, '*.zip'))

    # and process them
    with concurrent.futures.ThreadPoolExecutor(max_workers=100) as executor:
        results = executor.map(_process_zip, files)

        # write results
        for result in results:
            with open(levels_res_file, 'a') as f:
                f.writelines('"%s", %s\n' % r for r in result[0])
            with open(objects_res_file, 'a') as f:
                f.writelines('"%s", "%s"\n' % r for r in result[1])

    logger.info('Done. Result files written to "%s"', out_dir)


@click.command()
@click.option('--in_dir', default='work_dir', type=click.Path(), show_default=True, help='Input files directory')
@click.option('--out_dir', default='out', type=click.Path(), show_default=True, help='Output directory')
def cli(in_dir, out_dir):
    main(in_dir, out_dir)


if __name__ == '__main__':
    import sys

    logging.basicConfig(level=logging.INFO, format='%(message)s', stream=sys.stdout)
    cli()
