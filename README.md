# README

Тестовое задание.

Задача: написать программу на Python, которая делает следующие действия:

- Создает 50 zip-архивов, в каждом 100 xml файлов со случайными данными следующей структуры:
```xml
<root>
	<var name=’id’ value=’<случайное уникальное строковое значение>’/>
	<var name=’level’ value=’<случайное число от 1 до 100>’/>
	<objects>
		<object name=’<случайное строковое значение>’/>
		<object name=’<случайное строковое значение>’/>
		…
	</objects>
</root>
```
  В тэге objects случайное число (от 1 до 10) вложенных тэгов object.

- Обрабатывает директорию с полученными zip архивами, разбирает вложенные xml файлы и формирует 2 csv файла:
    - Первый: id, level - по одной строке на каждый xml файл
    - Второй: id, object_name - по отдельной строке для каждого тэга object (получится от 1 до 10 строк на каждый xml файл)

Очень желательно сделать так, чтобы задание 2 эффективно использовало ресурсы многоядерного процессора.
Также желательно чтобы программа работала быстро.


### Установка

Для работы требуется Python 3.4.  
Настрока virtualenv и установка зависимостей производится стандартно.
```
virtualenv env --no-site-packages -p <path-to-python-3.4>

# linux
source env/bin/activate

# windows
env\Scripts\activate.bat

pip install -r requirements.txt
```

Под windows могут возникнуть проблемы с установкой lxml из-за отстутствия компиляторов. В этом случае используйте команду
```
pip install click lxml-3.4.4-cp34-none-win_amd64.whl
```

### Запуск

Модуль generate.py содержит код для генерации сэмплов (первая часть задания).

Модуль main.py содержит код для обработки xml (вторая часть задания).

Запуск из командной строки:

```bash
python generate.py 
python main.py
```
Или с параметрами:
```bash
python generate.py --work_dir samples --archive_amount 50 --files_per_archive 100
python main.py --in_dir samples --out_dir out

python generate.py --help
python main.py --help
```

Запуск из python-скриптов:
```python
import generate
import main

generate.generate(work_dir='samples', archive_amount=50, files_per_archive=100)
main.main(in_dir='samples', out_dir='out')
```